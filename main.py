'''Copyright 2011 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
#limitations under the License.'''

from client import db_api
from client import matcher

from google.appengine.api import app_identity
from google.appengine.api import backends
from google.appengine.api import oauth
from google.appengine.api import urlfetch
from google.appengine.api import users

from google.appengine.api.urlfetch import fetch

import cgi
import datetime
import json
import logging
import os
import pprint as pp
import random
import sys
import traceback
import urllib
import webapp2


# constants
_DEBUG = True
_JSON_ENCODER = json.JSONEncoder()

if _DEBUG:
  _JSON_ENCODER.indent = 4
  _JSON_ENCODER.sort_keys = True

if backends.get_backend() == 'matcher':
  _match_maker = matcher.MatchMaker()

_EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email"
_IS_DEVELOPMENT = os.environ['SERVER_SOFTWARE'].startswith('Development/')

#######################################################################
# common functions
#######################################################################

def tojson(python_object):
  """Helper function to output and optionally pretty print JSON."""
  return _JSON_ENCODER.encode(python_object)


def fromjson(msg):
  """Helper function to ingest JSON."""
  try:
    return json.loads(msg)
  except Exception, e:
    raise Exception('Unable to parse as JSON: %s' % msg)

_PAIRING_KEY = fromjson(open('shared/pairing-key.json').read())['key']

def fetchjson(url, deadline, payload=None):
  """Fetch a remote JSON payload."""

  method = "GET"
  headers = {}
  if payload:
    method = "POST"
    headers['Content-Type'] = 'application/json'
  result = fetch(url, method=method, payload=payload, headers=headers, deadline=deadline).content
  return fromjson(result)


def json_by_default_dispatcher(router, request, response):
  """WSGI router which defaults to 'application/json'."""

  response.content_type = 'application/json'
  return router.default_dispatcher(request, response)


def e(msg):
  """Convient method to raise an exception."""

  raise Exception(repr(msg))


def w(msg):
  """Log a warning message."""

  logging.warning('##### %s' % repr(msg))





#######################################################################
# frontend related stuff
#######################################################################




# issue Json
class JsonAssets(webapp2.RequestHandler):

  def get(self, filename):
    f = open(filename, 'r')
    self.response.write(f.read())
    f.close()


#######################################################################




# handler common to frontends and backends
handlers = [
    ('/(shared/.*\.json)', JsonAssets)
]


app = webapp2.WSGIApplication(handlers, debug=True)
app.router.set_dispatcher(json_by_default_dispatcher)
