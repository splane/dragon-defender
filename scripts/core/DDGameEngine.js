/*Copyright 2011 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
#limitations under the License.*/

DDGameEngineClass = Class.extend({

//-- SOME OF THESE CAME FROM THE PARENT CLASS
  clearColor: '#000000',
  gravity: 10,
  screen: {
    x: 0,
    y: 0
  },

  entities: [],
  namedEntities: {},
  backgroundAnims: {},

  spawnCounter: 0,
  gameScore: 0,
  gameWave: 1,
  dragonCount: 0,
  difficulty: 0, //0 Normal, 1 Hard

  
  timeSinceGameUpdate: 0,
  timeSincePhysicsUpdate: 0,
  clock: null,

  _deferredKill: [],
 
  
  gMap: null,

  gDragonRider: null,  //The Dragon Rider
  
  
  fps:0,
  currentTick:0,
  lastFpsSec:0,

 
  newMapPos: {x:0,y:0},

  init: function () {
    this.clock = new TimerClass();
  },
  //-----------------------------
  setup: function () {
  	

	//-- THIS FROM THE PARENT CLASS TO SET UP SOME PHYSICS
    gPhysicsEngine.create(Constants.PHYSICS_UPDATES_PER_SEC, false);
    gPhysicsEngine.addContactListener({
		BeginContact: function (idA, idB) {
    		// log('bc');
  		},
      	PostSolve: function (bodyA, bodyB, impulse) {	
			if (impulse < 0.1) return;
			// NB THE ORIGINAL PARENT HAD LOGIC TO DETERMINE IF THE
			// COLLISION WAS PLAYER - MISSILE - WALL etc
			gGameEngine.onCollisionTouch(bodyA,bodyB,impulse);
		}
    });

    //clm hax - force load a map
    this.gMap = new TileMapLoaderClass();
    this.gMap.load(map1);
	
	  	
	//-- THIS FROM THE OLD CLIENT GAME ENGINE
    gInputEngine.bind(gInputEngine.KEY.W, 'move-up');
    gInputEngine.bind(gInputEngine.KEY.S, 'move-down');
    gInputEngine.bind(gInputEngine.KEY.A, 'move-left');
    gInputEngine.bind(gInputEngine.KEY.D, 'move-right');
    gInputEngine.bind(gInputEngine.KEY.C, 'continue');
	gInputEngine.bind(gInputEngine.KEY.H, 'togglehard');
	gInputEngine.bind(gInputEngine.KEY.M, 'togglemusic');
	gInputEngine.bind(gInputEngine.KEY.F, 'togglesfx');

	// These don't need to be in separate directions but they may in the future
    gInputEngine.bind(gInputEngine.KEY.UP_ARROW, 'fire-up');
    gInputEngine.bind(gInputEngine.KEY.DOWN_ARROW, 'fire-down');
    gInputEngine.bind(gInputEngine.KEY.LEFT_ARROW, 'fire-left');
    gInputEngine.bind(gInputEngine.KEY.RIGHT_ARROW, 'fire-right');
    gInputEngine.bind(gInputEngine.KEY.SPACE, 'fire-down');
  },

  createNewGame: function() {
  	for (var i = 0; i < this.entities.length; i++) {
      var ent = this.entities[i];
      ent.kill();
	}
  	gGameEngine.gameScore = 0;
  	gGameEngine.gameWave = 0;
  	gGameEngine.dragonCount = 0;
 	gGameEngine.gDragonRider = gGameEngine.spawnEntity("Player", 400,300, null);

  },
  
  //-----------------------------
  onCollisionTouch: function(bodyA,bodyB,impulse)
  {
	if (impulse < 0.1) return;

	var uA = bodyA?bodyA.GetUserData():null;
	var uB = bodyB?bodyB.GetUserData():null;
	
	//Logger.log("Bang! BodyA: "+uA.id + " BodyB: "+uB.id);
	
	if (uA != null) {
	  if (uA.ent != null && uA.ent.onTouch) {
	    uA.ent.onTouch(bodyB, null, impulse);
	  }
	}
	if (uB != null) {
	  if (uB.ent != null && uB.ent.onTouch) {
	    uB.ent.onTouch(bodyA, null, impulse);
	  }
	}
  },
 
  getTime: function() { return this.currentTick * 0.05; },
  
  getEntityByName: function (name) {
    return this.namedEntities[name];
  },

  getEntityById: function(id) {
    for (var i = 0; i < this.entities.length; i++) {
      var ent = this.entities[i];
      if (ent.id == id) {
        return ent;
      }
    }
    return null;
  },

  getEntitiesByLocation: function (pos) {
    var a = [];
    for (var i = 0; i < this.entities.length; i++) {
      var ent = this.entities[i];
      if (ent.pos.x <= pos.x && ent.pos.y <= pos.y && (ent.pos.x + ent.size.x) > pos.x && (ent.pos.y + ent.size.y) > pos.y) {
        a.push(ent);
      }
    }
    return a;
  },

  getEntitiesWithinCircle: function(center, radius) {
    var a = [];
    for (var i = 0; i < this.entities.length; i++) {
      var ent = this.entities[i];
      var dist = Math.sqrt((ent.pos.x - center.x)*(ent.pos.x - center.x) + (ent.pos.y - center.y)*(ent.pos.y - center.y));
      if (dist <= radius) {
        a.push(ent);
      }
    }
    return a;
  },

  getEntitiesByType: function (typeName) {
    var entityClass = Factory.nameClassMap[typeName];
    var a = [];
    for (var i = 0; i < this.entities.length; i++) {
      var ent = this.entities[i];
      if (ent instanceof entityClass && !ent._killed) {
        a.push(ent);
      }
    }
    return a;
  },

  nextSpawnId: function () {
	return this.spawnCounter++;
  },

  onSpawned: function () {},
  onUnspawned: function () {},

  spawnEntity: function (typename, x, y, settings) {
    var entityClass = Factory.nameClassMap[typename];
    var es = settings || {};
    es.type = typename;
    var ent = new(entityClass)(x, y, es);
    var msg = "SPAWNING " + typename + " WITH ID " + ent.id;
    if (ent.name) {
      msg += " WITH NAME " + ent.name;
    }
    gGameEngine.entities.push(ent);	
    if (ent.name) {
      gGameEngine.namedEntities[ent.name] = ent;
    }
    gGameEngine.onSpawned(ent);
    //if (ent.type == "Player") {
    //	Logger.log("creating a player here")
    //}
    return ent;
  },


  removeEntity: function (ent) {
    if (!ent) return;

    this.onUnspawned(ent);

    // Remove this entity from the named entities
    if (ent.name) {
      delete this.namedEntities[ent.name];
    }

    // We can not remove the entity from the entities[] array in the midst
    // of an update cycle, so remember all killed entities and remove
    // them later.
    // Also make sure this entity doesn't collide anymore and won't get
    // updated or checked
    ent._killed = true;

    this._deferredKill.push(ent);
  },


  //-----------------------------
  update: function () {
    this.currentTick++;


    if (!this.gDragonRider || this.gDragonRider.isDead) {
      return;
    }

    // entities
    for (var i = 0; i < this.entities.length; i++) {
      var ent = this.entities[i];
      if (!ent._killed) {
        ent.update();
        
      }
    }
    // remove all killed entities
    for (var i = 0; i < this._deferredKill.length; i++) {
      this.entities.erase(this._deferredKill[i]);
    }
    this._deferredKill = [];

    var inputEngine = gInputEngine;

    //logic
    var pInput = {
      x: 0,
      y: 0,
      flying: false,
      fire: false,
    };
    var move_dir = new Vec2(0, 0);
    if (gInputEngine.state('move-up'))
      move_dir.y -= 1;
    if (gInputEngine.state('move-down'))
      move_dir.y += 1;
    if (gInputEngine.state('move-left'))
      move_dir.x -= 1;
    if (gInputEngine.state('move-right'))
      move_dir.x += 1;
    if (move_dir.LengthSquared()) {
      pInput.flying = true;

      move_dir.Normalize();
      //move_dir.Multiply(this.gDragonRider.flightSpeed);

      pInput.x += move_dir.x * this.gDragonRider.flightSpeed;
      pInput.y += move_dir.y * this.gDragonRider.liftSpeed;

    } else {
      pInput.flying = false;
      pInput.x = 0;
      pInput.y = 0;
    }

    //var dPX = gRenderEngine.getScreenPosition(this.gDragonRider.pos).x;
    //var dPY = gRenderEngine.getScreenPosition(this.gDragonRider.pos).y;

    // Keyboard based facing & firing direction
    if (gInputEngine.state('fire-up') || 
    	gInputEngine.state('fire-down') ||
		gInputEngine.state('fire-left') ||
		gInputEngine.state('fire-right'))
        pInput.fire = true;


    // Record and send out inputs
    this.gDragonRider.pInput = pInput;
	
    //recenter our map bounds based upon the player's centered position
    // need a sensible recentering strategy
    var pPos = this.gDragonRider.physBody.GetPosition();
    //this.newMapPos.x = pPos.x - (this.gMap.viewRect.w * 0.5);

    this.newMapPos.x = this.gDragonRider.pos.x - (this.gMap.viewRect.w * 0.5);
    //this.newMapPos.y = this.gDragonRider.pos.y - (this.gMap.viewRect.h * 0.5);

	if(gInputEngine.state('togglesfx')) {
		gSM._muteFX = !gSM._muteFX;
		gInputEngine.clearState('togglesfx');
	}
	if(gInputEngine.state('togglemusic')) {
		gSM.togglemuteBG();
		gInputEngine.clearState('togglemusic');
	}
	if(gInputEngine.state('togglehard')) {
		this.difficulty = !this.difficulty;
		gInputEngine.clearState('togglehard');
	}

  },

	//-----------------------------  
  updatePhysics: function() {
    gPhysicsEngine.update();
    
    //SHOULDN'T THIS BE IN ENTITY UPDATE SO ALL ENTITIES ARE UPDATED?
    var pPos = this.gDragonRider.physBody.GetPosition();
    //var pVel = this.gDragonRider.physBody.GetLinearVelocity();
	//    Logger.log("pPos.x " + pPos.x + " pPos.y " + pPos.y);
	//    Logger.log("pVel.x " + pVel.x + " pVel.y " + pVel.y);
    this.gDragonRider.pos.x = pPos.x;
    this.gDragonRider.pos.y = pPos.y;
  },
  
  checkTheWaveSituation: function() {
  	if(gGameEngine.dragonCount == 0){
  		gGameEngine.gameWave++;
  		for( var i = 0; i < gGameEngine.gameWave+1; i++ ) {
			var distToDragonRider = 0;
			while(distToDragonRider < 200) {
  				var sx = Math.floor(50+2900*Math.random());
  				var sy = Math.floor(50+400*Math.random());
				var oCenter = this.gDragonRider.pos;
			
				var dx = Math.abs(oCenter.x - sx);
				var dy = Math.abs(oCenter.y - sy);
				distToDragonRider = Math.sqrt(dx*dx + dy*dy);
			}			
			fd = (Math.random()>0.5)?1:-1;
			dragonChoice = 3*Math.random();
			if(dragonChoice < 1) dragonColour = "BlueDragon";
			else if(dragonChoice < 2) dragonColour = "YellowDragon"; 
			else dragonColour = "RedDragon";
  	 		gGameEngine.spawnEntity(dragonColour, sx, sy, {flightDirection:fd }); //use settings to determine a random direction
  	 	}
  	}	
  },
  
	//-----------------------------
  run: function() {
    this.fps++;
    GlobalTimer.step();

    var timeElapsed = this.clock.tick();
    this.timeSinceGameUpdate += timeElapsed;
    this.timeSincePhysicsUpdate += timeElapsed;

	if (!this.gDragonRider || this.gDragonRider.isDead) {
		// OOPS - DragonRider est mort.
		var ctx = gRenderEngine.context;
		gRenderEngine.drawString("GAME OVER", "VT323", 250, 200, 56, {color:'yellow'});	
		gRenderEngine.drawString("press C to play again", "VT323", 250, 300, 25, {color:'yellow'});

    	var inputEngine = gInputEngine;
    	if (gInputEngine.state('continue'))
    		this.createNewGame();	
		if(gInputEngine.state('togglemusic')) {
			gSM.togglemuteBG();
			gInputEngine.clearState('togglemusic');
		}    
	gInputEngine.clearPressed();
		return;
	}
    else this.checkTheWaveSituation();

    while (this.timeSinceGameUpdate >= Constants.GAME_LOOP_HZ &&
      this.timeSincePhysicsUpdate >= Constants.PHYSICS_LOOP_HZ) {
      // JJG: We should to do a physics update immediately after a game update to avoid
      //      the case where we draw after a game update has run but before a physics update

      this.update();
      this.updatePhysics();
      this.timeSinceGameUpdate -= Constants.GAME_LOOP_HZ;
      this.timeSincePhysicsUpdate -= Constants.PHYSICS_LOOP_HZ;

    }

    while (this.timeSincePhysicsUpdate >= Constants.PHYSICS_LOOP_HZ) {
      // JJG: Do extra physics updates
      this.updatePhysics();
      this.timeSincePhysicsUpdate -= Constants.PHYSICS_LOOP_HZ;
    }

    if(this.lastFpsSec < this.currentTick/Constants.GAME_UPDATES_PER_SEC && this.currentTick % Constants.GAME_UPDATES_PER_SEC == 0) {
      this.lastFpsSec = this.currentTick / Constants.GAME_UPDATES_PER_SEC;
      this.fps = 0;
    }
	
    var fractionOfNextPhysicsUpdate = this.timeSincePhysicsUpdate / Constants.PHYSICS_LOOP_HZ;

	this.update(); //CLM why was this removed?

	
    this.draw(fractionOfNextPhysicsUpdate);
    gInputEngine.clearPressed();
  },


  //-----------------------------
  on_collision: function (msg) {
		var ent0 = this.getEntityByName(msg.ent0);
		var ent1 = this.getEntityByName(msg.ent1);
		if(ent0 == null) ent0 = this.getEntityById(msg.ent0);
		if(ent1 == null) ent1 = this.getEntityById(msg.ent1);
		var body0 = null;
		var body1 = null;
		
		if(ent0 != null)
			body0 = ent0.physBody;
		if(ent1 != null)
			body1 = ent1.physBody;
		
		
		this.onCollisionTouch(body0,body1,msg.impulse);
		
	},

//need some functions to do the following:
// 1. reset the game
// 2. create a new dragon rider
// 3. spawn a dragon

  //-----------------------------
  draw: function (fractionOfNextPhysicsUpdate) {

    var ctx = gRenderEngine.context;

    // Alpha-beta filter on camera
    this.gMap.viewRect.x = parseInt(alphaBeta(this.gMap.viewRect.x, this.newMapPos.x, 0.9));
    this.gMap.viewRect.y = parseInt(alphaBeta(this.gMap.viewRect.y, this.newMapPos.y, 0.9));

    ctx.fillStyle = "black";
	ctx.fillRect ( 0 , 0 , gRenderEngine.canvas.width, gRenderEngine.canvas.height );

    // Draw map.
    this.gMap.draw(null);

	//draw the heading
    ctx.strokeStyle = "purple";
    ctx.fillStyle = "rgb(30,30,30)";
	ctx.fillRect(250,0,300,100);
	var img = loadAtlasImage("img/dd_mini.png");
	ctx.drawImage(img,250,25);
	ctx.strokeRect(250,0,300,100);


	gRenderEngine.drawString("score: "+gGameEngine.gameScore, "VT323", 50, 50, 18, {color:'yellow'});
	gRenderEngine.drawString("wave : "+gGameEngine.gameWave, "VT323", 50, 75, 18, {color:'yellow'});

	//draw health and controls
	gRenderEngine.drawString("mode: "+((this.difficulty)?"hard":"easy"), "VT323", 575, 10, 8, {color:'green'});
	gRenderEngine.drawString("music: "+((gSM._muteSound)?"off":"on"), "VT323", 575, 20, 8, {color:'green'});
	gRenderEngine.drawString("sFX: "+((gSM._muteFX)?"off":"on"), "VT323", 575, 30, 8, {color:'green'});
		
	gRenderEngine.drawString("health:", "VT323", 575, 50, 18, {color:'yellow'});
	health = this.gDragonRider._health;
	if(health > 750) ctx.fillStyle = "green";
	else if(health > 250) ctx.fillStyle = "rgb(255,102,0)"
	else ctx.fillStyle = "red";
	if(health > 0)
		ctx.fillRect(575,65,(health/1000)*150,20);
	ctx.strokeStyle = "yellow";
	ctx.strokeRect(575,65,150,20);

    // Bucket entities by zIndex
	var fudgeVariance = 128;
    var zIndex_array = [];
    var entities_bucketed_by_zIndex = {}
    this.entities.forEach(function(entity) {
    	
    	var scanx = entity.pos.x;
    	var scany = entity.pos.y;
    	switch(entity.id) {
    		case "DragonRider":
    			ctx.fillStyle = "green";
    		    break;
			case "BlueDragon":
				ctx.fillStyle = "blue";
				break;
			case "RedDragon":
				ctx.fillStyle = "red";
				break;
			case "YellowDragon":
				ctx.fillStyle = "yellow";
				break;
			case "FireStreak":
				ctx.fillStyle = "rgb(255,102,0)";
				break;
			default:	
    			ctx.fillStyle = "white";
    	}
    	ctx.fillRect(250+(scanx/10), 25+(scany/10), 2, 2);  //MORE MAGIC NUMBERS RASING TECH DEBT
    	
		//don't draw entities that are off screen
		if(	entity.pos.x >= gGameEngine.gMap.viewRect.x-fudgeVariance && entity.pos.x < gGameEngine.gMap.viewRect.x+gGameEngine.gMap.viewRect.w+fudgeVariance &&
			entity.pos.y >= gGameEngine.gMap.viewRect.y-fudgeVariance && entity.pos.y < gGameEngine.gMap.viewRect.y+gGameEngine.gMap.viewRect.h+fudgeVariance)
		{				
			if (zIndex_array.indexOf(entity.zIndex) === -1) 
			{
				zIndex_array.push(entity.zIndex);
				entities_bucketed_by_zIndex[entity.zIndex] = [];
			}
			entities_bucketed_by_zIndex[entity.zIndex].push(entity);
		}
		
    });
	
    // Draw entities sorted by zIndex
    zIndex_array.sort(function (a,b) {return a - b;});
    zIndex_array.forEach(function(zIndex) {
      entities_bucketed_by_zIndex[zIndex].forEach(function(entity) {
				entity.draw(fractionOfNextPhysicsUpdate);
      });
    });
  },


	//-----------------------------		
	preloadComplete:false,
	preloadAssets:function ()
	{
		//go load images first
		var assets = new Array();

		assets.push("img/ddsprites.png");
		assets.push("img/dd_mini.png");

		//maps!
		var map = map1;
		for (var i = 0; i < map.tilesets.length; i++) 
			assets.push("img/" + map.tilesets[i].image.replace(/^.*[\\\/]/, ''));
		
		//sounds
		if(!gSM._unsupported) {
			assets.push("sound/DST_AngryRobotIII_converted_mono.ogg");
			assets.push("sound/30248_streety_sword7_converted.ogg");
			assets.push("sound/85568_joelaudio_dragon_roar.ogg");
			assets.push("sound/102733_sarge4267_explosion3_converted.ogg");
		}
//SF		assets.push("./sound/bounce0.ogg");
//SF		assets.push("./sound/energy_pickup.ogg");
//SF		assets.push("./sound/explode0.ogg");
//SF		assets.push("./sound/grenade_shoot0.ogg");
//SF		assets.push("./sound/item_pickup0.ogg");
//SF		assets.push("./sound/machine_shoot0.ogg");
//SF		assets.push("./sound/quad_pickup.ogg");
//SF		assets.push("./sound/rocket_shoot0.ogg");
//SF		assets.push("./sound/shield_activate.ogg");
//SF		assets.push("./sound/shotgun_shoot0.ogg");
//SF		assets.push("./sound/spawn0.ogg");
//SF		assets.push("./sound/sword_activate.ogg");
		
		loadAssets(assets, function()
		{
			//Logger.log("beginning loadAssets");
			xhrGet("img/ddsprites.json", false,
				function(data){
					var obj = JSON.parse(data.response);
						
					var sheet = new SpriteSheetClass();
					gSpriteSheets['ddsprites'] =sheet;
					sheet.load("img/ddsprites.png");
						
					for (var key in obj.frames)
					{
						var val = obj.frames[key];
						var cx=-val.frame.w * 0.5;
						var cy=-val.frame.h * 0.5;
						
						if(val.trimmed)
						{
							cx = val.spriteSourceSize.x - (val.sourceSize.w * 0.5);
							cy = val.spriteSourceSize.y - (val.sourceSize.h * 0.5);
							
						}
						
						sheet.defSprite(key, val.frame.x, val.frame.y, val.frame.w, val.frame.h, cx, cy);
					}
					//Logger.log("preload done!");										
					gGameEngine.preloadComplete = true;
				});
		});
		
			
	
		//effects!@
		
	
	},

});


gGameEngine = new DDGameEngineClass();

