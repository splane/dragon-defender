/*Copyright 2011 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
#limitations under the License.*/

DragonRiderClass = EntityClass.extend({
	flightSpeed: 200*5,//200 52*5, //got this from Player
	liftSpeed: 60*5, //60
	flying: true,
	flightDirection: 1, // 1 right, 0 left 
	physBody: null,
	numKills: 0,
	isDead:false,
	zIndex: 8,
  	pInput: {x:0, y:0, fire:false},
  	_lastFirestreak: 0,
  	_health: 1000,
  	_flightSpriteAnimList: [],


  init: function (inputx, inputy, settings) {

    this.parent(inputx, inputy, settings);
	this.id = "DragonRider";

    var entityDef = {
      id: "player",
      x: this.pos.x,
      y: this.pos.y,
      halfHeight: 13, // Naughty: this.hsize.y / 2, //JJG: divide by 2 to let the player squeeze through narrow corridors
      halfWidth: 26, // Naughty: this.hsize.x / 2,
      damping: 0,
      angle: 0,
      categories: ['player'],
      collidesWith: ['all'],
      userData: {
        "id": "player",
        "ent": this
      }
    };

	//Logger.log("Created DragonRider with x,y "+entityDef.x + " " + entityDef.y);
	this.physBody = gPhysicsEngine.addBody(entityDef);

    var pPos = this.physBody.GetPosition();
    this.pos.x = pPos.x;
    this.pos.y = pPos.y;

  
   //-- Setting up the sprites	
   var names=["dragonrider_l","dragonrider_r"];
   for(var q=0; q < names.length; q++)
   {
	   var sheet_down = new SpriteSheetAnimClass();
	   sheet_down._animIncPerFrame = 0.1; //SF slowbeat of wing
	   sheet_down.loadSheet('ddsprites',"img/ddsprites.png");
		for(var i =1; i < 4; i++)
			sheet_down.pushFrame(names[q] + i + ".png");
		this._flightSpriteAnimList.push(sheet_down);
	}

  },

  takeDamage: function(dmg) {
  	this._health -= dmg;
  	if(this._health < 0)
  		this.isDead = true;
  },

	//-----------------------------------------
  update: function () {
 
	//what anim should I be playing?  if both keys pressed then right wins
    if (gInputEngine.state('move-left'))
      this.flightDirection = 0;
    if (gInputEngine.state('move-right'))
      this.flightDirection = 1;


    var pPos = this.physBody.GetPosition();
    this.pos.x = pPos.x;
    this.pos.y = pPos.y;

	if(this.pInput) {    	

    	// if we are firing then create a firestreak
    	if(this.pInput.fire && (gGameEngine.getTime()-this._lastFirestreak > 0.5)) {
    		var dir = new Vec2(this.flightDirection==1?1:-1, 0);
    		if(this.flightDirection==0){
    			anim = "firestreak_l";
    			xoffset = -100;
    			yoffset = 6; //Magic Numbers
			}
    		else{
    			anim = "firestreak_r";
    			xoffset = 100;
    			yoffset = 6; //Magic Numbers
    		} 
    		//Logger.log("firestreak launched!");
			gGameEngine.spawnEntity("FireStreak", this.pos.x+xoffset - gGameEngine.gMap.viewRect.x, this.pos.y+yoffset - gGameEngine.gMap.viewRect.y, {
				dir: dir,
				pos: {x:this.pos.x+xoffset, y:this.pos.y+yoffset},
				lifetimeInSeconds:0.5,
				speed:900,
				animFrameName: anim,
				impactFrameName: '150',
				spawnSound: "./sound/85568_joelaudio_dragon_roar.ogg",
				impactSound: "./sound/102733_sarge4267_explosion3_converted.ogg"
				    				
			}); 
			this._lastFirestreak = gGameEngine.getTime();   			
    			
    	}
	}
    this.pInput.fire = false;
   

	var vx = this.pInput.x;
	var vy = this.pInput.y;
	if(Math.abs(vx)+Math.abs(vy) > 0 || gGameEngine.difficulty == 0)
		this.physBody.SetLinearVelocity(new Vec2(vx, vy));

    this.parent();

  },

  kill: function () {
    //remove my physics body
    gPhysicsEngine.removeBodyAsObj(this.physBody);
    this.physBody = null;
    //destroy me as an ent.
    gGameEngine.removeEntity(this);
  },

  //-----------------------------------------
  draw: function (fractionOfNextPhysicsUpdate) {
  
//	if(this.isDead) return;
	
//    var ctx = gRenderEngine.context;

//    var interpolatedPosition = {x:this.pos.x, y:this.pos.y};

    var pPos = this.pos;
    if (this.physBody) pPos = this.physBody.GetPosition();

    var interpolatedPosition = {x:pPos.x, y:pPos.y};

	if(this.physBody) {
	  var velocity = this.physBody.GetLinearVelocity();
      interpolatedPosition.x += (velocity.x * Constants.PHYSICS_LOOP_HZ) * fractionOfNextPhysicsUpdate;
      interpolatedPosition.y += (velocity.y * Constants.PHYSICS_LOOP_HZ) * fractionOfNextPhysicsUpdate;
    }


    var dPX = gRenderEngine.getScreenPosition(interpolatedPosition).x;
    var dPY = gRenderEngine.getScreenPosition(interpolatedPosition).y;
	
	//this._drawPlayerAvatar(ctx, {player:this, locX:dPX, locY:dPY});
	//this._flightSpriteAnimList[this.flightDirection].draw(interpolatedPosition.x,interpolatedPosition.y,{noMapTrans:true});	
	this._flightSpriteAnimList[this.flightDirection].draw(dPX,dPY,{noMapTrans:true});	

  },

// onTouch: function (otherBody, point, impulse) {
		
//	if (!this.physBody || otherBody==null) return false;
//    var physOwner = otherBody.GetUserData().id;
//    switch(physOwner) {
//    	case 'dragon':
//    		Logger.log("A Dragon!");
//    		this.takeDamage(this.GetUserData().ent.crashDmg);
//    		otherBody.GetUserData().ent.markForDeath = true;
//    		break;
//    	default:
//    		return true;
//    }    
    
//    return true;	
//  },


});

Factory.nameClassMap["Player"] = DragonRiderClass;
