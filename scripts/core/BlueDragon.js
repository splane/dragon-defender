/*Copyright 2011 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
#limitations under the License.*/

BlueDragonClass = EntityClass.extend({
	flightSpeed: 52*5,//52*5, //got this from Player
	flying: true,
	flightDirection: 1, // 1 right, -1 left 
	physBody: null,
	numKills: 0,
	isDead:false,
	zIndex: 8,
  	pInput: {x:0, y:0, fire:false},
	crashDmg: 10,
	pointValue: 150,
	_lastshove: 0,
	_lastfireball: 0,
  	_flightSpriteAnimList: [],


  init: function (inputx, inputy, settings) {

    this.parent(inputx, inputy, settings);
	this.id = "BlueDragon";
	this.flightDirection = settings.flightDirection;

	var guid = newGuid_short();
    var entityDef = {
      id: "BlueDragon"+guid,
      x: this.pos.x,
      y: this.pos.y,
      halfHeight: 13, // Naughty: this.hsize.y / 2, //JJG: divide by 2 to let the player squeeze through narrow corridors
      halfWidth: 26, // Naughty: this.hsize.x / 2,
      damping: 0,
      angle: 0,
      categories: ['dragon'],
      collidesWith: ['boundary','firestreak'],
      userData: {
        "id": "BlueDragon",
        "ent": this
      }
    };

	//New dragon so lets add one to the game counter - this isn't a great way to do waves, but it's quick for testing
	gGameEngine.dragonCount++;

	this.physBody = gPhysicsEngine.addBody(entityDef);

    var pPos = this.physBody.GetPosition();
    this.pos.x = pPos.x;
    this.pos.y = pPos.y;

  
   //-- Setting up the sprites	
   var names=["bluedragon_l","bluedragon_r"];
   for(var q=0; q < names.length; q++)
   {
	   var sheet_down = new SpriteSheetAnimClass();
	   sheet_down._animIncPerFrame = 0.1; //SF slowbeat of wing
	   sheet_down.loadSheet('ddsprites',"img/ddsprites.png");
		for(var i =1; i < 4; i++)
			sheet_down.pushFrame(names[q] + i + ".png");
		this._flightSpriteAnimList.push(sheet_down);
	}

	this.randomShove();
	//this.physBody.SetLinearVelocity(new Vec2(vx*this.flightSpeed, vy*this.flightSpeed));
  

  },

  kill: function () {
    //remove my physics body
    gPhysicsEngine.removeBodyAsObj(this.physBody);
    this.physBody = null;
    //destroy me as an ent.
    gGameEngine.removeEntity(this);

	//I think I must be dead!  so one less on the wave
	gGameEngine.dragonCount--;

  },
  
  randomShove: function() {
	// generate a random number between 0 and 1
	var randValue = Math.random();
	randValue = randValue * (100+(300*Math.random())); // scale it by a number between 50 and 100
	var randAngle = 2*Math.PI*Math.random();

//	this.physBody.ApplyForce(new Vec2(Math.cos(randAngle) * randValue, Math.sin(randAngle) * randValue), this.physBody.GetPosition());  // move the player
	var vx = Math.cos(randAngle) * randValue;
	var vy = Math.sin(randAngle) * randValue;
	this.physBody.SetLinearVelocity(new Vec2(vx,vy));

	this._lastshove = gGameEngine.getTime();

    if(vx > 0)
      this.flightDirection = 1;
    else
      this.flightDirection = -1;


	//Logger.log("random shove-> value:" + randValue + " angle:" + randAngle*(180/Math.PI));
  },

	//-----------------------------------------
  update: function () {
 
    var pPos = this.physBody.GetPosition();
    this.pos.x = pPos.x;
    this.pos.y = pPos.y;

	if(this.pos.x > 3000 || this.pos.x < 0 || this.pos.y > 500 || this.pos.y < 0)
		this.markForDeath = true; // glitch - need to resolve
	
	if(gGameEngine.getTime()-this._lastshove > 25) this.randomShove();
   
	if( Math.random() < 0.01 && (gGameEngine.getTime()-this._lastfireball > 25)) {  // 20 % chance of sending out a shot
		this.shootFire();
		this._lastfireball = gGameEngine.getTime()
	}

   
    this.parent();

  },

  shootFire: function() {

    var point1 = this.pos;
    
    var dir = new Vec2(this.flightDirection, (1-2*Math.random()));
    point1.x += dir.x * 20;
    point1.y += dir.y * 20;


    var ent = gGameEngine.spawnEntity("BouncingFireBall", point1.x - gGameEngine.gMap.viewRect.x, point1.y - gGameEngine.gMap.viewRect.y, {
		pos:point1,
		dir:dir,
		speed:150,
		animFrameName: "fireball",
		lifetimeInSeconds: 300,
		//impactFrameName: 'yes',
		spawnSound: "./sound/85568_joelaudio_dragon_roar.ogg",
		impactSound: "./sound/102733_sarge4267_explosion3_converted.ogg"
		
		});
  },

  //-----------------------------------------
  draw: function (fractionOfNextPhysicsUpdate) {
  
	if(this.isDead) return;
	
    var pPos = this.pos;
    if (this.physBody) pPos = this.physBody.GetPosition();

    var interpolatedPosition = {x:pPos.x, y:pPos.y};

	if(this.physBody) {
	  var velocity = this.physBody.GetLinearVelocity();
      interpolatedPosition.x += (velocity.x * Constants.PHYSICS_LOOP_HZ) * fractionOfNextPhysicsUpdate;
      interpolatedPosition.y += (velocity.y * Constants.PHYSICS_LOOP_HZ) * fractionOfNextPhysicsUpdate;
    }

    var dPX = gRenderEngine.getScreenPosition(interpolatedPosition).x;
    var dPY = gRenderEngine.getScreenPosition(interpolatedPosition).y;
	
	this._flightSpriteAnimList[(this.flightDirection==1)?1:0].draw(dPX,dPY,{noMapTrans:true});	

  },

  //  onTouch: function (otherBody, point, impulse) {
		
//	if (!this.physBody || otherBody==null) return false;
//    var physOwner = otherBody.GetUserData().id;
//    switch(physOwner) {
//    	case 'player':
//    		Logger.log("A Player!");
//    		otherBody.GetUserData().ent.takeDamage(this.crashDmg);
//    		this.markForDeath = true;
//    		break;
//    		break;
//    	default:
//    		return true;
//    }    
//    
//    return true;	
//  },


});

Factory.nameClassMap["BlueDragon"] = BlueDragonClass;
