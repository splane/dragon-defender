/*Copyright 2012 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
#limitations under the License.*/

ElectroBallClass = WeaponInstanceClass.extend({
  lifetime: 0,
  physBody: null,
  dir: null,
  _spriteAnim: null,
  _impactFrameName: null,
  _impactSound:null,
  _speed:100,
  _dmgAmt:100,
  
 init: function (x, y, settings) {
	this.parent(x, y, settings);
 	this.id = "ElectroBall";
	var startPos = settings.pos;
    this.dir = {x:settings.dir.x, y:settings.dir.y};
	this.lifetime = 1;	//in seconds

	if(settings)
	{
		if(settings.speed)
			this._speed = settings.speed;
		if(settings.lifetimeInSeconds)
			this.lifetime = settings.lifetimeInSeconds;
		if(settings.impactSound)
			this._impactSound = settings.impactSound;
		if(settings.impactFrameName)
			this._impactFrameName = settings.impactFrameName;
	}

    
    var guid = newGuid_short();
    //create our physics body;
    var entityDef = {
      id: "ElectroBall" + guid,
      x: startPos.x,
      y: startPos.y,
      halfHeight: 4,
      halfWidth: 4,
      damping: 0,
      angle: 0,
      categories: ['ElectroBall'],
      collidesWith: ['player', 'boundary'],
      userData: {
        "id": "wpnElectroBall" + guid,
        "ent": this
      }
    };
    this.physBody = gPhysicsEngine.addBody(entityDef);

    this.physBody.SetLinearVelocity(new Vec2(settings.dir.x * this._speed, settings.dir.y * this._speed));
	
	this.zIndex = 20;
	this._spriteAnim = new SpriteSheetAnimClass();
	this._spriteAnim.loadSheet('ddsprites',"img/ddsprites.png");

	if(settings)
	{
		if(settings.animFrameName)
		{
			var spriteNames = getSpriteNamesSimilarTo(settings.animFrameName);
			for(var i =0; i < spriteNames.length; i++)
				this._spriteAnim.pushFrame(spriteNames[i]);
		}
				

		if(settings.spawnSound)
		{
			gSM.playSound(settings.spawnSound,{looping:false,volume:0.1});
			//gGameEngine.playWorldSound(settings.spawnSound,x,y);
		}
	}

  },
 
  //--------------------------------------
  update: function () {
	
	if(this.pos.x > 3000 || this.pos.x < 0 || this.pos.y > 500 || this.pos.y < 0)
		this.markForDeath = true; // glitch - need to resolve
		
	this.lifetime-= 0.5;
    if (this.lifetime <= 0) {
      this.kill();
      return;
    }

    if (this.physBody != null) {
      var pPos = this.physBody.GetPosition();
      this.pos.x = pPos.x;
      this.pos.y = pPos.y;
    }

    this.parent();
	
  },
  //-----------------------------------------
  draw: function (fractionOfNextPhysicsUpdate) {
    var gMap = gGameEngine.gMap;
    
    //rotate based upon velocity
    var pPos = this.pos;
    if (this.physBody) pPos = this.physBody.GetPosition();

    var interpolatedPosition = {x:pPos.x, y:pPos.y};

    if(this.physBody) {
      var velocity = this.physBody.GetLinearVelocity();
      interpolatedPosition.x += (velocity.x * Constants.PHYSICS_LOOP_HZ) * fractionOfNextPhysicsUpdate;
      interpolatedPosition.y += (velocity.y * Constants.PHYSICS_LOOP_HZ) * fractionOfNextPhysicsUpdate;
    }

	this._spriteAnim.draw(interpolatedPosition.x,interpolatedPosition.y,{});
	
  },
   //--------------------------------------
  kill: function () {
    //remove my physics body
    gPhysicsEngine.removeBodyAsObj(this.physBody);
    this.physBody = null;
    //destroy me as an ent.
    gGameEngine.removeEntity(this);
  },
//--------------------------------------
  sendUpdates: function () {
    this.sendPhysicsUpdates();
  },
//--------------------------------------
  onTouch: function (otherBody, point, impulse) {
		
	if (!this.physBody || otherBody==null) return false;
    var physOwner = otherBody.GetUserData().id;
    switch(physOwner) {
    	case 'player':
    		//Logger.log("Ouch that hurt!");
    		otherBody.GetUserData().ent.takeDamage(this._dmgAmt);
    		this.markForDeath = true;
    		break;
    	case 'boundary':
    		this.markForDeath = true;
    		break;
    	default:
    		this.markForDeath = false;
    }    
    
    return true;
		
		
  },
//--------------------------------------
});

Factory.nameClassMap['ElectroBall'] = ElectroBallClass;
