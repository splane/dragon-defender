/*Copyright 2012 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
#limitations under the License.*/

FireStreakClass = WeaponInstanceClass.extend({
  lifetime: 0,
  physBody: null,
  dir: null,
  _spriteAnim: null,
  _impactFrameName: null,
  _impactSound:null,
  _speed:800,
  _dmgAmt:10,
  
 init: function (x, y, settings) {
	this.parent(x, y, settings);
 	this.id = "FireStreak";
	var startPos = settings.pos;
    this.dir = {x:settings.dir.x, y:settings.dir.y};
	this.lifetime = 1;	//in seconds
    var spd = 800;    
	if(settings)
	{
		if(settings.speed)
			this._speed = settings.speed;
		if(settings.lifetimeInSeconds)
			this.lifetime = settings.lifetimeInSeconds;
		if(settings.impactSound)
			this._impactSound = settings.impactSound;
		if(settings.impactFrameName)
			this._impactFrameName = settings.impactFrameName;
	}

    
    var guid = newGuid_short();
    //create our physics body;
    var entityDef = {
      id: "FireStreak" + guid,
      x: startPos.x,
      y: startPos.y,
      halfHeight: 2,
      halfWidth: 75,
      damping: 0,
      angle: 0,
      categories: ['firestreak'],
      collidesWith: ['dragon', 'boundary'],
      userData: {
        "id": "wpnFireStreak" + guid,
        "ent": this
      }
    };
    this.physBody = gPhysicsEngine.addBody(entityDef);

    this.physBody.SetLinearVelocity(new Vec2(settings.dir.x * this._speed, settings.dir.y * this._speed));
	
	this.zIndex = 20;
	this._spriteAnim = new SpriteSheetAnimClass();
	this._spriteAnim.loadSheet('ddsprites',"img/ddsprites.png");

	if(settings)
	{
		if(settings.animFrameName)
		{
			var name = settings.animFrameName;
			//var sprites = getSpriteNamesSimilarTo(settings.animFrameName);
			for(var i =1; i < 7; i++){
			
				this._spriteAnim.pushFrame(settings.animFrameName+i+'.png');
				//Logger.log("name: "+settings.animFrameName+i+'.png');
			}
		}
				

		if(settings.spawnSound)
		{
			gSM.playSound(settings.spawnSound,{looping:false,volume:0.1});
			//gGameEngine.playWorldSound(settings.spawnSound,x,y);
		}
	}

  },
 
  //--------------------------------------
  update: function () {
	
    this.lifetime-=0.05;
    if (this.lifetime <= 0) {
      this.kill();
      return;
    }

    this.physBody.SetLinearVelocity(new Vec2(this.dir.x * this._speed,
                                             this.dir.y * this._speed));

    if (this.physBody != null) {
      var pPos = this.physBody.GetPosition();
      this.pos.x = pPos.x;
      this.pos.y = pPos.y;
    }

    this.parent();
  },
  //-----------------------------------------
  draw: function (fractionOfNextPhysicsUpdate) {
    var gMap = gGameEngine.gMap;
    
    //rotate based upon velocity
    var pPos = this.pos;
    if (this.physBody) pPos = this.physBody.GetPosition();

    var interpolatedPosition = {x:pPos.x, y:pPos.y};

    if(this.physBody) {
      var velocity = this.physBody.GetLinearVelocity();
      interpolatedPosition.x += (velocity.x * Constants.PHYSICS_LOOP_HZ) * fractionOfNextPhysicsUpdate;
      interpolatedPosition.y += (velocity.y * Constants.PHYSICS_LOOP_HZ) * fractionOfNextPhysicsUpdate;
    }

	this._spriteAnim.draw(interpolatedPosition.x,interpolatedPosition.y,{});
	
  },
   //--------------------------------------
  kill: function () {
    //remove my physics body
    gPhysicsEngine.removeBodyAsObj(this.physBody);
    this.physBody = null;
    //destroy me as an ent.
    gGameEngine.removeEntity(this);
  },
//--------------------------------------
  sendUpdates: function () {
    this.sendPhysicsUpdates();
  },
//--------------------------------------
  onTouch: function (otherBody, point, impulse) {
    if (!this.physBody) return false;

    if (otherBody ==null) return false; //invalid object??
    //var physOwner = otherBody.GetUserData().ent;
    var physOwner = otherBody.GetUserData().id;
	var physEnt = otherBody.GetUserData().ent;

    //Logger.log("Bye bye: "+physOwner);

	if( (physOwner != 'boundary') && !physEnt.markForDeath)
	{
		physEnt.markForDeath = true;
    
    	var value = physEnt.pointValue;
    	gGameEngine.gameScore += value;

    	//spawn impact visual - but should be related to the score! so a bit of a cludge here
		if(this._impactFrameName)
		{
			//var pPos = this.physBody.GetPosition();
			var pPos = physEnt.pos;
			var efct = gGameEngine.spawnEntity("InstancedEffect", pPos.x, pPos.y, null);
			efct.onInit({x:pPos.x,y:pPos.y},
				{	
					playOnce:false,
					lifeInSeconds:3, 
					//similarName:this._impactFrameName, 
					similarName:""+value,
					uriToSound:this._impactSound 
				});
		}
	}
							
    this.markForDeath = true;

    return true; //return false if we don't validate the collision
  },
//--------------------------------------
});

Factory.nameClassMap['FireStreak'] = FireStreakClass;
